public class Kolejka
{

	Kibic[] kolejka;
	int count = 0;
	int przod = 0;
	int koniec = 0;
	

	Kolejka(int pojemnosc)
	{
		kolejka = new Kibic[pojemnosc];
		for(int i=0;i<pojemnosc;i++)
			kolejka[i] = new Kibic();
	}
	/*
	 * Dodawanie kibica na koniec kolejki
	 */
	public synchronized int dodaj(int v)
	{
		int index;
		kolejka[koniec].druzyna = v;
		index = koniec;
		koniec = (koniec + 1) % kolejka.length;
		count++;
		//System.out.println(">> Przod " + przod + " koniec " + koniec + " count " + count);
		notifyAll();
		return index;
	}
	/*
	 * Usuwanie kibica z początku kolejki
	 */
	public synchronized int opusc()
	{
		while (count == 0)  						//kolejka jest pusta
		{
			try
			{
				wait();
			} catch (InterruptedException e)
			{

			}
		}
		int x = kolejka[przod].druzyna;
		kolejka[przod] = new Kibic();
		count--;
		przod = (przod + 1) % kolejka.length;
		//System.out.println("<< Przod " + przod + " koniec " + koniec + " count " + count);
		notifyAll();
		return x;
	}
	/*
	 * Usuwanie kibica z dowolnego miejsca w kolejce
	 */
	public synchronized int opusc(int v)
	{
		while (count == 0)
		{
			try
			{
				wait();
			} catch (InterruptedException e)
			{

			}
		}
		
		for (int i = przod, counter = 0; counter < count; i = (i + 1) % kolejka.length, counter++)
		{
			if(kolejka[i].priorytet == 5 && kolejka[i].druzyna != v) 			//Kibic przepuścił już 5 kibiców innej drużyny
			{
				System.out.println("PRIORYTET");
				return 0;
			}
			else if (kolejka[i].druzyna == v)									//Znaleziono kibica aktualnie sprawdzanej drużyny
			{
				for (int j = i, c = 0; c < count - counter - 1 ; j++, c++)		//Przesuwanie kolejki do przodu
				{
					kolejka[j] = kolejka[j + 1];	
				}
				for(int l = przod;l < i;l++)									//Dodanie priorytetu kibicom którzy przepuścili kogoś z innej drużyny.
					kolejka[l].priorytet++;
				koniec--;
				kolejka[koniec] = new Kibic();									//Referencja :)
				count--;
				return v;
			}
		}
		
		return 0;
	}

	public int length()
	{
		return count;
	}
	
	public int getDruzynaKibica(int index)
	{
		return kolejka[(przod+index)%kolejka.length].druzyna;
	}
	
	public Kibic getKibicAt(int index)
	{
		return kolejka[(przod+index)%kolejka.length];
	}
}
