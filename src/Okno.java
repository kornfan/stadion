import java.util.logging.Level;
import java.util.logging.Logger;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.*;

public class Okno extends BasicGame
{
	static int SZER = 640;
	static int WYS = 480;

	private static int Liczba_bramek = 2;
	private static int Pojemnosc = 50;

	private Bramka[] bramki;
	private Kolejka kolejka;
	private Stadion pociag;
	private Image im_bramka;
	private Image im_druzyna1;
	private Image im_druzyna2;

	public Okno(String gamename)
	{
		super(gamename);
	}

	@Override
	public void init(GameContainer gc) throws SlickException
	{

		im_bramka = new Image("res/bramka.png");
		im_bramka = im_bramka.getScaledCopy(0.5f);
		im_druzyna1 = new Image("res/druzyna1.png");
		im_druzyna1 = im_druzyna1.getScaledCopy(0.15f);
		im_druzyna2 = new Image("res/druzyna2.png");
		im_druzyna2 = im_druzyna2.getScaledCopy(0.1f);

		gc.getGraphics().setBackground(new Color(170, 170, 170));

		bramki = new Bramka[Liczba_bramek];
		kolejka = new Kolejka(Pojemnosc);
		pociag = new Stadion(kolejka, Pojemnosc);
		pociag.start();

		for (int i = 0; i < Liczba_bramek; i++)
		{
			bramki[i] = new Bramka(kolejka);
			bramki[i].start();
		}

	}

	@Override
	public void update(GameContainer gc, int i) throws SlickException
	{
	}

	@Override
	public void render(GameContainer gc, Graphics g) throws SlickException
	{
		
		
		float x = 200;
		float y = WYS/2;
		
		for (int i = 1; i <= Liczba_bramek; i++)
		{
			
			float height = i * (WYS / (Liczba_bramek + 1));
			im_bramka.drawCentered(x, height);
			g.drawLine(x, height, x + 200, height);
			
			for (int j=0;j < bramki[i-1].getKolejka().length();j++)
			 {
				float wysokosc = 0;
				if(j==0) wysokosc = height - 50;
				if(j==1) wysokosc = height;
				if(j==2) wysokosc = height + 50;
				if(bramki[i-1].getKolejka().getDruzynaKibica(j) == -1)
				{
					im_druzyna1.drawCentered(100,wysokosc);
				}
				else
					im_druzyna2.drawCentered(100,wysokosc);
				
				Polygon cut = new Polygon();
				cut.addPoint(100, wysokosc);
				
				for(int k = 0; k < bramki[i-1].getKolejka().getKibicAt(j).status;k++)
				{
					cut.addPoint(100+(float)Math.cos(Math.toRadians(k))*10, wysokosc+(float)Math.sin(Math.toRadians(k)) * 10);
				}
				g.fill(cut);
				g.draw(cut);
			 }
			
		}
		
		/*
		 * wyświetlanie głównej kolejki	
		 */
		
		for (int j=0;j < kolejka.length();j++)
		 {
//		  g.drawOval(x+35*j, y, 30, 30);
			 if(kolejka.getDruzynaKibica(j) == -1)
			 {
				 im_druzyna1.drawCentered(x+35*j, y);
				 
			 }
			 else
				 im_druzyna2.drawCentered(x+35*j,y);
		 }

	}

	public static void main(String[] args)
	{
		try
		{
			AppGameContainer appgc;
			appgc = new AppGameContainer(new Okno("Simple Slick Game"));
			appgc.setDisplayMode(SZER, WYS, false);
			appgc.start();
		} catch (SlickException ex)
		{
			Logger.getLogger(Okno.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
}