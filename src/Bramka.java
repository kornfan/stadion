public class Bramka extends Thread
{
	private final int MAX = 3;						//maksymalna liczba kibicow jednoczesnie przeszukiwanych
	private int aktywna_druzyna = 0;				//kibice tej druzyny moga wchodzic do bramki
	private Kolejka kolejka; 						//referencja do głównej kolejki
	private Kolejka aktywni = new Kolejka(MAX);		//kibice aktualnie przeszukiwani				//
	public Bramka(Kolejka kolejka)
	{
		this.kolejka = kolejka;
	}
	
	public void run()
	{
		while (true)
		{
			if (aktywni.length() == 0) 				// jeśli bramka jest pusta - weź pierwszego kibica z początku kolejki
			{
				aktywna_druzyna = kolejka.opusc();
				int index = aktywni.dodaj(aktywna_druzyna);
				System.out.println(">dodalem1 " + aktywna_druzyna);
				Ochroniarz ochroniarz = new Ochroniarz(aktywni,index);    //Rozpoczęcie przeszukiwania
				ochroniarz.start();
			} else if(aktywni.length() >= MAX)
			{
				while (aktywni.length() >= MAX)		// wszystkie stanowiska w bramce są zajęte
				{
					try
					{
						System.out.println("Czekam");
						Thread.sleep(1000);
					} catch (InterruptedException e)
					{

					}
				}
			}
			else									// w bramce już jest co najmniej jeden kibic
			{

				
				int druzyna = kolejka.opusc(aktywna_druzyna);		//poszkiwany jest kibic należący do aktualnie przeszukiwanej druzyny
				if(druzyna != 0)					// znaleziono kibica
				{
					int index = aktywni.dodaj(druzyna);
					System.out.println(">dodalem2 " + aktywna_druzyna);
					Ochroniarz ochroniarz = new Ochroniarz(aktywni,index);
					ochroniarz.start();
				}
				else 								//Nie ma w kolejce kibiców aktualnej drużyny lub trzeba przepuścić kibica przeciwnej drużyny
				{
					try
					{
						System.out.println("Czekam na "+ aktywna_druzyna);
						Thread.sleep(1000);
					} catch (InterruptedException e)
					{
					}
				}
				
			}
		}
	}
	public Kolejka getKolejka()
	{
		return aktywni;
	}

}
