
public class Stadion extends Thread{
	
	private int pojemnosc;
	private Kolejka kolejka;
	public Stadion(Kolejka kolejka, int cap)
	{
		pojemnosc = cap;
		this.kolejka = kolejka;
	}
	/*
	 * Wątek dodaje nowych kibiców do kolejki w odstępie 0 - 1000 msec.
	 * 
	 */
	public void run()
	{
		for (int i = 0; i < pojemnosc; i++)
		{
			try
			{
				Thread.sleep((long) (1000 * Math.random() ));
			} catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			int val = Math.random() < 0.5 ? -1 : 1;
			// int val = t[i];
			kolejka.dodaj(val); // -1 - druzyna pierwsza, 1 - druzyna druga
			System.out.println("dodałem " + val);
		}
	}

	
}
